$(function() {
    function WorkflowViewModel(parameters) {
        var self = this;

        self.printmodels = ko.observableArray([1,2,3,4]);
        self.tvcollections = ko.observableArray([]);
        self.tvthings = ko.observableArray([]);

        self.activeStage = ko.observable({});
        self.stages = [{
            id: "rts",
            title: "Ready to splice",
            template: 'stage-rts-template'
        },{
            id: "rtp",
            title: "Ready to print",
            template: 'stage-other-template'
        },{
            id: "printing",
            title: "Printing...",
            template: 'stage-other-template'
        },{
            id: "print_done",
            title: "Print done",
            template: 'stage-other-template'
        }];

        // this will hold the URL currently displayed by the iframe
        //self.models = ko.observableArray(self._models);

        //self.goToUrl = function() {
            //self.currentUrl(self.newUrl());
        //};

        // This will get called before the HelloWorldViewModel gets bound to the DOM, but after its
        // dependencies have already been initialized. It is especially guaranteed that this method
        // gets called _after_ the settings have been retrieved from the OctoPrint backend and thus
        // the SettingsViewModel been properly populated.
        //self.onBeforeBinding = function() {
            //self.models(self._models);
        //}

        self.setStage = function(newStage) {
            console.log("Setting stage to "+newStage["id"]);
            self.activeStage(newStage);
        }

        self.displayStage = function(stage) {
            console.log("Displaying stage "+stage().template);
            return stage().template;
        }

        self.fetchFromCollection = function(collectionId) {

            var payload = {
                groupid: collectionId,
            };

            $.ajax({
                url: "/plugin/workflow/mprovider/thingiverse/groups",
                type: "GET",
                dataType: "json",
                contentType: "charset=UTF-8",
                error: function(response) {
                    console.log("Failed to fetch collections from thingiverse");
                },
                success: function(response) {
                    console.log("Full response: "+JSON.stringify(response));
                    $.each(response, function(i, tvcol) {
                        self.tvthings.push(tvcol);
                    });
                },
            });
        }

        self.fetchCollections = function() {

            var payload = {
                access_token: "cc52bb212ea2bdf526fb33e8583d703c",
            };

            $.ajax({
                url: "https://api.thingiverse.com/" + "users/wouter92/collections/true/",
                type: "GET",
                dataType: "json",
                data: payload,
                contentType: "charset=UTF-8",
                error: function(response) {
                    console.log("Failed to fetch collections from thingiverse");
                },
                success: function(response) {
                    console.log("Full response: "+JSON.stringify(response));
                    $.each(response, function(i, tvcol) {
                        self.tvcollections.push(tvcol);
                        self.fetchFromCollection(tvcol.id);
                    });
                },
            });
        }

        self.onBeforeBinding = function() {
            self.fetchCollections();
            self.setStage(self.stages[0]);
        }
    }

    // This is how our plugin registers itself with the application, by adding some configuration
    // information to the global variable OCTOPRINT_VIEWMODELS
    OCTOPRINT_VIEWMODELS.push({
        // This is the constructor to call for instantiating the plugin
        construct: WorkflowViewModel,
        elements: ["#tab_plugin_workflow"]
    });
});
