import requests
from .modelprovider import GenericModelProvider

class Thingiverse(GenericModelProvider):

    API_ROOT = "https://api.thingiverse.com"
    TOKEN = "cc52bb212ea2bdf526fb33e8583d703c"

    def __init__(self):
        self.username = "wouter92"

    def login(self, username, password):
        pass

    def getUserGroups(self):
        payload = {"access_token":Thingiverse.TOKEN}
        url = "{root}/users/{user}/collections/true".format(root=Thingiverse.API_ROOT, user=self.username)
        response = requests.get(url, params=payload)
        return response.json()

    def getModelsByGroup(self, groupId=None):
        result = []
        groupIds = []

        if groupId:
            groupIds = [groupId]
        else:
            groupIds = [g["id"] for g in self.getUserGroups()]

        for gid in groupIds:
            payload = {"access_token":Thingiverse.TOKEN}
            url = "{root}/collections/{group}/things".format(root=Thingiverse.API_ROOT, group=gid)
            result = result + requests.get(url, params=payload).json()
            return result

    def getModelsByGroupName(self, groupName):
        if groupName:
            for group in self.getUserGroups():
                if group["name"] == groupName:
                    return getModelsByGroup(group["id"])

        return []
