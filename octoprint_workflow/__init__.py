import octoprint.plugin
from .libs.model import Model
from .libs.modelproviders.thingiverse import Thingiverse
from flask import jsonify, make_response, Response, request
import json
import http

http.client.HTTPConnection.debuglevel = 1

class WorkflowPlugin(octoprint.plugin.StartupPlugin,
                       octoprint.plugin.TemplatePlugin,
                       octoprint.plugin.AssetPlugin,
                       octoprint.plugin.BlueprintPlugin):

    def __init__(self):
        self.mproviders = {}
        self.mproviders["thingiverse"] = Thingiverse()

    # StartupPlugin

    def on_after_startup(self):
        self._logger.info("Hello World!")

    # TemplatePlugin

    def get_template_configs(self):
        return [
            dict(type="settings", custom_bindings=False)
        ]

    # BlueprintPlugin

    @octoprint.plugin.BlueprintPlugin.route("/mprovider/<mprovider>/groups", methods=["GET"])
    def getModelsByGroup(self, mprovider):
        if "groupid" in request.values:
            groupId = request.values["groupid"]
        else:
            groupId = None
        result = self.mproviders[mprovider].getModelsByGroup(groupId)
        if result:
            return make_response(jsonify(result))
        return make_response('', 404)

    # AssetPlugin

    def get_assets(self):
        return dict(
            js=["js/workflow.js"],
            css=["css/workflow.css"],
            less=["less/workflow.less"]
        )

__plugin_pythoncompat__ = ">=3,<4"
__plugin_implementation__ = WorkflowPlugin()

